const express = require('express');

const socketIO = require("socket.io");

const http = require('http');

const path = require('path');


const { Usuarios } = require("./clases/usuarios")
const { crearMensaje } = require("./utilidades/utilidades");

const app = express();

let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));


let io = socketIO(server);

const usuarios = new Usuarios();

io.on("connection", (client) => {
    console.log("Usuario conectado");
    
    client.on("entrarChat", (data, callback) => {
        if (!data.nombre) {
            return callback({
                error:true,
                mensaje:"El nombre es necesario",
            });
        }
        let personas= usuarios.agregarPersona(client.id, data.nombre);
        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
        callback(personas);
    });

    client.on("crearMensaje", (data) => {
        let persona = usuarios.getPersona(client.id)
        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.emit("crearMensaje", mensaje);
    })
    
    client.on("disconnect", () => {
        let personaBorrada = usuarios.borrarPersona(client.id);

        client.broadcast.emit("crearMensaje", {
            usuario: "Administrador",
            mensaje: crearMensaje('Admin', `${personaBorrada.nombre} salio`),
        });
        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
    });
});


server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${port}`);

});