var socket = io();

socket.on('connect', function () {
  console.log('Conectado al servidor');
});

socket.on('disconnect', function () {
  console.log('Perdimos conexion con el servidor');
});


//Enviar informacion
socket.emit('enviarMensaje', {
    usuario: 'Alvaro',
    mensaje: 'Hola mundo',
  });
  
//Escuchas informacion
socket.on('enviarMensaje', function (mensaje) {
    console.log('Mensaje del servidor', mensaje);
  });
  